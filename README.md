#funda assignment

This application assumes a working installation of IEx and an authenticated AWS CLI with the ability to get attributes for the queues that need monitoring

Ideally I'd have used CloudWatch alerts but as they where not available on the test account, I chose Elixir for its superior concurency primitives.



Possible extensions:

- multiple monitoring services (only SQS supported now)
- multiple alerting mechanisms (only logging supported now)

to run:
```
cd config_manager
mix deps.get
iex -S mix run
```
