defmodule ConfigManager.Mixfile do
  use Mix.Project

  def project do
    [
      app: :config_manager,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :team_alerting, :sqs_monitor],
      mod: {ConfigManager.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:team_alerting, path: "../team_alerting"},
      {:sqs_monitor, path: "../sqs_monitor"},
    ]
  end
end
