defmodule ConfigManager.ConfigReader do

  alias SqsMonitor
  alias TeamAlerting

  def apply(config_path) do
    config_path
    |> File.read()
    |> (fn {:ok, body} -> body end).()
    |> Poison.decode()
    |> create_teams()

    config_path
  end

  defp create_teams({:ok, config_file}) do
    config_teams = parse_teams(config_file)
    current_teams = TeamAlerting.get_teams()

    current_teams
    |> MapSet.difference(config_teams)
    |> Enum.each(&TeamAlerting.remove_team/1)

    config_teams
    |> MapSet.difference(current_teams)
    |> Enum.each(&TeamAlerting.create_team/1)

    Enum.each(config_teams, fn teamname -> create_alerts(teamname, config_file) end)

    config_file
    |> create_monitors()

    {:ok, config_file}
  end

  defp create_monitors(config_teams) do
    config_monitors = parse_monitors(config_teams)
    current_monitors = MapSet.new(SqsMonitor.get_monitors())

    config_monitors
    |> MapSet.difference(current_monitors)
    |> Enum.each(&SqsMonitor.start/1)

    current_monitors
    |> MapSet.difference(config_monitors)
    |> Enum.each(&SqsMonitor.stop/1)

    config_teams
  end

  defp parse_monitors(config_teams = %{ "teams" => teams}) do
    teams
    |> Enum.flat_map(fn %{"teamname" => teamname} -> parse_alerts(teamname, config_teams) end)
    |> Enum.map(fn {uri, _limit} -> uri end)
    |> MapSet.new()
  end

  defp create_alerts(teamname, config_file) do
    config_alerts = parse_alerts(teamname, config_file)
    teamname
    |> TeamAlerting.get_alerts()
    |> MapSet.new
    |> MapSet.difference(config_alerts)
    |> Enum.each(fn {uri, _value} -> TeamAlerting.remove_alert(teamname, uri) end)

    config_alerts
    |> Enum.each(fn {uri, value} -> TeamAlerting.add_alert(teamname, uri, value) end)

  end

  defp parse_teams(%{ "teams" => teams}) do
    teams
    |> Enum.map(fn %{ "teamname" => teamname} -> teamname end)
    |> MapSet.new()
  end

  defp parse_alerts(teamname, %{"teams" => teams}) do
    teams
    |> Enum.filter(fn team -> filter_alerts(team, teamname) end)
    |> Enum.flat_map(fn %{"alerts" => alerts} -> alerts end)
    |> Enum.map(fn %{"queue_name" => queue_name, "max_acceptable_value" => max} -> {"775912293446/#{queue_name}", max} end)
    |> MapSet.new()
  end

  defp parse_alerts(_teamname, _config_file) do
    MapSet.new()
  end

  def filter_alerts(%{"teamname" => teamname1}, teamname2)
  when teamname1 == teamname2 do
    true
  end

  def filter_alerts(_, _) do
    false
  end


end
