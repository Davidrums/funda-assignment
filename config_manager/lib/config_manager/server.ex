defmodule ConfigManager.Server do
  use GenServer, start: {__MODULE__, :start_link, []}, restart: :transient

  alias ConfigManager.ConfigReader

  @reload_interval 1000

  def start_link() do
    GenServer.start_link(__MODULE__, "../config.json", name: __MODULE__)
  end

  def handle_info(:reload, config_path) do
    Process.send_after(self(), :reload, @reload_interval)
    ConfigReader.apply(config_path)
    {:noreply, config_path}
  end

  def init(config_path) do
    Process.send_after(self(), :reload, @reload_interval)
    ConfigReader.apply(config_path)
    {:ok, config_path}
  end
end
