defmodule ConfigManager do
  def apply_config(config_path) do
    config_path
    |> File.read()
  end
end
