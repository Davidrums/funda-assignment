defmodule TeamAlerting.Team do
  def new(teamname) do
    %{
      teamname: teamname,
      alerts: [],
    }
  end

  def add_alert(state = %{alerts: alerts}, {uri, triggervalue}) do
    :ok = SqsMonitor.subscribe(uri)
    cleaned_alerts = remove_alert_from_state(alerts, uri)
    new_alerts = [{uri, triggervalue} | cleaned_alerts]
    %{state | alerts: new_alerts}
  end

  def get_alerts(%{alerts: alerts}) do
    alerts
  end

  def remove_alert(state = %{alerts: alerts}, to_remove) do
    :ok = SqsMonitor.unsubscribe(to_remove)
    new_alerts = remove_alert_from_state(alerts, to_remove)
    %{state | alerts: new_alerts}
  end

  def check_measurement(state = %{teamname: teamname, alerts: alerts}, {sqs, value}) do
    alerts
    |> Enum.filter(fn {uri, trigger} -> sqs == uri && value > trigger end)
    |> Enum.map(fn {uri, trigger} -> IO.puts "Alert triggered to #{teamname} for #{uri} because #{value} is greater than #{trigger}" end)
    state
  end

  defp remove_alert_from_state(alerts, to_remove) do
    alerts
    |> Enum.filter(fn {uri, _value} -> uri != to_remove end)
  end
end
