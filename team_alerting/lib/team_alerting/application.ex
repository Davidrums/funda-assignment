defmodule TeamAlerting.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      {Registry, keys: :unique, name: Registry.Teams},
      TeamAlerting.TeamSupervisor,
    ]

    opts = [strategy: :one_for_one, name: TeamAlerting.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
