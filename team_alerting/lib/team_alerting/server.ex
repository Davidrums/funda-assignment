defmodule TeamAlerting.Server do
  use GenServer, start: {__MODULE__, :start_link, []}, restart: :transient

  alias TeamAlerting.Team

  def start_link(teamname) when is_binary(teamname) do
    GenServer.start_link(__MODULE__, teamname, name: via_tuple(teamname))
  end

  def via_tuple(teamname) do
    {:via, Registry, {Registry.Teams, teamname}}
  end

  def init(teamname) do
    {:ok, Team.new(teamname)}
  end

  def handle_call(:get_alerts, _from, state) do
    state
    |> Team.get_alerts()
    |> MapSet.new()
    |>( fn alerts -> {:reply, alerts, state} end).()
  end

  def handle_call(:get_teamname, _from, state = %{teamname: teamname}) do
    {:reply, teamname, state}
  end

  def handle_cast({:add_alert, to_add}, state) do
    state
    |> Team.add_alert(to_add)
    |> noreply()
  end

  def handle_cast({:remove_alert, to_add}, state) do
    state
    |> Team.remove_alert(to_add)
    |> noreply()
  end

  def handle_info({:measurement, value}, state) do
    state
    |> Team.check_measurement(value)
    |> noreply
  end

  defp noreply(state) do
    {:noreply, state}
  end

end
