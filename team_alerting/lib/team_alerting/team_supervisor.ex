defmodule TeamAlerting.TeamSupervisor do
  use Supervisor

  alias TeamAlerting.Server

  def start_link(_options), do:
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)

  def start_team(teamname), do:
    Supervisor.start_child(__MODULE__, [teamname])

  def stop_team(teamname) do
    Supervisor.terminate_child(__MODULE__, pid_from_name(teamname))
  end

  def init(:ok), do:
    Supervisor.init([Server], strategy: :simple_one_for_one)

  def pid_from_name(teamname) do
    teamname
    |> Server.via_tuple()
    |> GenServer.whereis()
  end
end
