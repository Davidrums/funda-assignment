defmodule TeamAlerting do

  import TeamAlerting.TeamSupervisor, only: [pid_from_name: 1]

  defdelegate create_team(teamname), to: TeamAlerting.TeamSupervisor, as: :start_team
  defdelegate remove_team(teamname),  to: TeamAlerting.TeamSupervisor, as: :stop_team

  def add_alert(teamname, uri, trigger) do
    teamname
    |> pid_from_name()
    |> GenServer.cast({:add_alert, {uri, trigger}})
  end

  def remove_alert(teamname, uri) do
    teamname
    |> pid_from_name()
    |> GenServer.cast({:remove_alert, uri})
  end

  def get_alerts(teamname) do
    teamname
    |> pid_from_name()
    |> GenServer.call(:get_alerts)
  end

  def get_teams() do
    Supervisor.which_children(TeamAlerting.TeamSupervisor)
    |> Enum.map(fn {_, pid, _, _} -> get_teamname(pid) end)
    |> MapSet.new()
  end

  defp get_teamname(pid) do
    GenServer.call(pid, :get_teamname)
  end
end
