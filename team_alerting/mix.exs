defmodule TeamAlerting.Mixfile do
  use Mix.Project

  def project do
    [
      app: :team_alerting,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :sqs_monitor],
      mod: {TeamAlerting.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:sqs_monitor, path: "../sqs_monitor"}
    ]
  end
end
