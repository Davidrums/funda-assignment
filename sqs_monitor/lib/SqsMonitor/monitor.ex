defmodule SqsMonitor.Monitor do

  def new(uri) do
    %{
      uri: uri,
    }
  end

  def monitor(%{uri: uri}=_state) do
    uri
    |> ExAws.SQS.get_queue_attributes([:approximate_number_of_messages])
    |> ExAws.request
    |> extract_body()
  end

  defp extract_body({:ok, %{body: %{attributes: %{approximate_number_of_messages: value}}}}) do
    value
  end
end
