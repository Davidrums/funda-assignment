#---
# Excerpted from "Functional Web Development with Elixir, OTP, and Phoenix",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material,
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose.
# Visit http://www.pragmaticprogrammer.com/titles/lhelph for more book information.
#---
defmodule SqsMonitor.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      {Registry, keys: :unique, name: Registry.SqsMonitor},
      supervisor(Phoenix.PubSub.PG2, [SqsMonitor.PubSub, []]),
      SqsMonitor.MonitorSupervisor,
    ]

    opts = [strategy: :one_for_one, name: SqsMonitor.Application]
    Supervisor.start_link(children, opts)
  end
end
