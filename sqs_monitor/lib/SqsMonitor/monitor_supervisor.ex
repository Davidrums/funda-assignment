defmodule SqsMonitor.MonitorSupervisor do
  use Supervisor

  alias SqsMonitor.Server

  def start_link(_options), do:
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)

  def start_monitor(uri), do:
    Supervisor.start_child(__MODULE__, [uri])

  def stop_monitor(uri) do
    Supervisor.terminate_child(__MODULE__, pid_from_name(uri))
  end

  def init(:ok), do:
    Supervisor.init([Server], strategy: :simple_one_for_one)

  defp pid_from_name(uri) do
    uri
    |> Server.via_tuple()
    |> GenServer.whereis()
  end
end
