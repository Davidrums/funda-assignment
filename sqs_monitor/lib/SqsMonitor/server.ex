defmodule SqsMonitor.Server do
  use GenServer, start: {__MODULE__, :start_link, []}, restart: :transient

  alias SqsMonitor.Monitor
  alias Phoenix.PubSub

  @monitor_interval 1_000

  def start_link(uri) when is_binary(uri) do
    GenServer.start_link(__MODULE__, uri, name: via_tuple(uri))
  end

  def via_tuple(uri) do
    {:via, Registry, {Registry.SqsMonitor, uri}}
  end

  def init(uri) do
    Process.send_after(self(), :monitor, @monitor_interval, [])
    {:ok, Monitor.new(uri)}
  end

  def handle_call(:get_uri, _from, state = %{uri: uri }) do
    {:reply, uri, state}
  end

  def handle_info(:monitor, state = %{uri: uri }) do
    state
    |> Monitor.monitor()
    |> ( fn value -> PubSub.broadcast(SqsMonitor.PubSub, uri, {:measurement, {uri, value}}) end).()

    Process.send_after(self(), :monitor, @monitor_interval, [])
    {:noreply, state}
  end
end
