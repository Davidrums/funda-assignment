defmodule SqsMonitor do

  defdelegate start(uri), to: SqsMonitor.MonitorSupervisor, as: :start_monitor
  defdelegate stop(uri),  to: SqsMonitor.MonitorSupervisor, as: :stop_monitor

  def subscribe(uri) do
    Phoenix.PubSub.subscribe(SqsMonitor.PubSub, uri)
  end

  def unsubscribe(uri) do
    Phoenix.PubSub.unsubscribe(SqsMonitor.PubSub, uri)
  end

  def get_monitors() do
    Supervisor.which_children(SqsMonitor.MonitorSupervisor)
    |> Enum.map(fn {_, pid, _, _} -> get_uri(pid) end)
  end

  defp get_uri(pid) do
    GenServer.call(pid, :get_uri)
  end
end
