defmodule SqsMonitor.Mixfile do
  use Mix.Project

  def project do
    [
      app: :sqs_monitor,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {SqsMonitor.Application, []},
      applications: [:ex_aws, :hackney, :sweet_xml, :poison],
      extra_applications: [:logger],
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_aws, "~> 1.1.5"},
      {:configparser_ex, "~> 0.2.1"},
      {:hackney, "~> 1.9"},
      {:poison, "~>1.2.0"},
      {:sweet_xml, "~>0.6"},
      {:phoenix_pubsub, "~> 1.0"},
    ]
  end
end
